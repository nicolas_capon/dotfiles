#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1="\[$(tput bold)\]\[\033[38;5;33m\]\u\[$(tput sgr0)\]\[\033[38;5;14m\]@\[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;33m\]\h\[$(tput sgr0)\]\[\033[38;5;11m\][\w]\[$(tput sgr0)\]\[\033[38;5;14m\]:\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"
set -o vi
alias vim='nvim'
alias vi='nvim'
alias tree='tree -CL 2'
alias wiki='nvim ~/vimwiki/index.wiki'
alias ll="ls -la"
PATH="$HOME/.local/bin:$PATH"
