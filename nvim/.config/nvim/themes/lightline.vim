let g:lightline = {'colorscheme': 'powerlineish', }
set laststatus=2

set noshowmode

set background=dark
set termguicolors

let g:deepspace_italics=1
let g:lightline = { 'colorscheme': 'deepspace' }
" Default theme background is too bright, remove it
autocmd ColorScheme * highlight Normal ctermbg=none guibg=none
