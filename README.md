# Dotfiles

A list of config files and scripts for linux.

## Install
1. Clone the repository
```
git clone --recursive https://gitlab.com/nicolas_capon/dotfiles.git ~/.dotfiles
```
2. Install gnu stow
3. Start importing the config files
```
stow bash
```
